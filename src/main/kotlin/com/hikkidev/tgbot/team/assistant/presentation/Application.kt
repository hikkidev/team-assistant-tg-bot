/*
 * Copyright 2021, Sora Nai
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.hikkidev.tgbot.team.assistant.presentation

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.LoggerContext
import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.command
import com.github.kotlintelegrambot.dispatcher.message
import com.github.kotlintelegrambot.dispatcher.newChatMembers
import com.github.kotlintelegrambot.dispatcher.telegramError
import com.github.kotlintelegrambot.entities.*
import com.github.kotlintelegrambot.entities.keyboard.KeyboardButton
import com.github.kotlintelegrambot.extensions.filters.Filter
import com.github.kotlintelegrambot.logging.LogLevel
import com.github.kotlintelegrambot.network.fold
import com.github.kotlintelegrambot.webhook
import com.hikkidev.tgbot.team.assistant.data.model.*
import com.hikkidev.tgbot.team.assistant.data.model.MessageId
import com.hikkidev.tgbot.team.assistant.data.repository.ChatRoomRepository
import com.hikkidev.tgbot.team.assistant.data.repository.ChatRoomRepositoryImpl
import com.hikkidev.tgbot.team.assistant.data.repository.MemberRepository
import com.hikkidev.tgbot.team.assistant.data.repository.MemberRepositoryImpl
import com.hikkidev.tgbot.team.assistant.domain.UseCaseFactory
import com.hikkidev.tgbot.team.assistant.domain.error.*
import com.hikkidev.tgbot.team.assistant.domain.model.toId
import com.hikkidev.tgbot.team.assistant.domain.usecase.MAX_TEAMS_PER_CHAT
import com.hikkidev.tgbot.team.assistant.domain.usecase.TEAMS_PER_ROW
import io.ktor.utils.io.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.litote.kmongo.Id
import org.litote.kmongo.coroutine.CoroutineClient
import org.litote.kmongo.coroutine.commitTransactionAndAwait
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.util.*
import kotlin.system.exitProcess
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

const val ACTION_TAG = "[Tag]"
const val ACTION_JOIN = "[Join]"
const val ACTION_LIST = "[List]"
const val ACTION_LEAVE = "[Leave]"
const val ACTION_PROTECT = "[Protect]"
const val KEY_CANCEL = "Отмена"
const val ICON_PROTECT = '*'

val logger: Logger = LoggerFactory.getLogger("Application")
val loggerContext = LoggerFactory.getILoggerFactory() as LoggerContext
val dataBaseLogger = loggerContext.getLogger("org.mongodb.driver").apply {
    level = Level.ERROR
}

val temporalMessageStorage = mutableMapOf<RoomId, MutableMap<MemberId, MutableList<MessageId>>>()

@OptIn(ExperimentalTime::class)
fun main() {
    val config = AppConfig()
    val certificateUtils = CertificateUtils(config)

    val client: CoroutineClient = KMongo.createClient(config.dbConnectionString).coroutine
    val factory = createFactory(client, config.dbName)
    val bot = createBot(config, certificateUtils, factory, client)
    val httpsServer = HttpsServer(config, certificateUtils)

    try {
        httpsServer.createServer {
            bot.processUpdate(it)
        }
        // Remove webhook, it sometimes fails the set if there is a previous webhook
        bot.deleteWebhook()
        val result = bot.startWebhook()
        logger.info("Webhook is started?:$result")
        bot.sendMessage(ChatId.fromId(config.ownerId), "WebhookIsEnabled:$result | Launching https server...")
        httpsServer.start()
    } catch (e: Throwable) {
        runBlocking {
            launch {
                val delay = Duration.minutes(5)
                bot.sendMessage(
                    ChatId.fromId(config.ownerId),
                    "Launch error - ${e.localizedMessage}.\nWaiting for credentials: $delay."
                )
                delay(delay)
                e.printStack()
                exitProcess(0)
            }
        }
    }
}

fun createFactory(client: CoroutineClient, dbName: String): UseCaseFactory {
    val database = client.getDatabase(dbName)
    val chatRepository: ChatRoomRepository = ChatRoomRepositoryImpl(database)
    val memberRepository: MemberRepository = MemberRepositoryImpl(database)
    return UseCaseFactory(chatRepository, memberRepository)
}

@OptIn(ExperimentalTime::class)
fun createBot(
    appConfig: AppConfig,
    certificateUtils: CertificateUtils,
    factory: UseCaseFactory,
    client: CoroutineClient
) = bot {
    token = appConfig.botToken
    logLevel = LogLevel.Error

    webhook {
        url = "https://${appConfig.hostIp}:${appConfig.port}/${appConfig.botToken}"
        /* This certificate argument is only needed when you want Telegram to trust your
        * self-signed certificates. If you have a CA trusted certificate you can omit it.
        * More info -> https://core.telegram.org/bots/webhooks */
        certificate = TelegramFile.ByFile(File(certificateUtils.certPath))
        maxConnections = 69
        allowedUpdates = listOf("message")
    }

    dispatch {
        command("start") {
            update.consume()
            bot.sendMessage(
                chatId = ChatId.fromId(message.chat.id),
                text = """
                    Привет! Общение 1 на 1 не продуктивно, вот почему:
                    1) Мой функционал полезен только там, где много людей
                    2) Чат должн быть зарегистрирован моим создателем, за подробностями обращайся к @hikkidev
                    
                    P.S. Меня всегда можно клонировать, я живу в ${appConfig.repoUrl}
                """.trimIndent()
            )
        }

        command("help") {
            update.consume()
            val markdownV2Text = """
                    *Доступные инструкции:*
                    `/i` Показать список команд в которых вы __*состоите*__
                    `/tag` Упомянуть участников определенной команды
                    `/join` Присоединиться к определенной команде
                    `/leave` Покинуть определенную команду
                    `/teams` Показать все существующие команды в чате
                    `/team_members` Показать всех участников определенной команды
                    
                    Подробное Readme: ${appConfig.repoUrl}
                """.trimIndent()
            bot.sendMessage(
                chatId = ChatId.fromId(message.chat.id),
                text = markdownV2Text,
                parseMode = ParseMode.MARKDOWN_V2
            )
        }

        command("register") {
            update.consume()
            message.runIfOwner(bot, appConfig.ownerId) {
                runBlocking {
                    factory.registerChatRoomUseCase(message.chat.id)
                        .fold({
                            bot.sendMessage(
                                chatId = ChatId.fromId(message.chat.id),
                                text = "Чат зарегистрирован.",
                                disableNotification = true,
                                replyToMessageId = message.messageId,
                                replyMarkup = ReplyKeyboardRemove()
                            )
                        }, {
                            message.logError(bot, it)
                        })
                }
            }
        }

        command("create") {
            update.consume()
            message.runIfOwner(bot, appConfig.ownerId) {
                val teamName = args.joinToString(separator = " ") {
                    it.replace(' ', '_').trim()
                }.replaceFirstChar {
                    if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString()
                }
                if (teamName.isBlank()) {
                    bot.sendMessage(
                        chatId = ChatId.fromId(message.chat.id),
                        text = "Требуется указать название новой команды!",
                        disableNotification = true,
                        replyToMessageId = message.messageId,
                        replyMarkup = ReplyKeyboardRemove()
                    )
                    return@command
                }
                if (teamName == KEY_CANCEL) {
                    bot.sendMessage(
                        chatId = ChatId.fromId(message.chat.id),
                        text = "Это имя зарезервировано.",
                        disableNotification = true,
                        replyToMessageId = message.messageId,
                        replyMarkup = ReplyKeyboardRemove()
                    )
                    return@command
                }
                if (!teamName.all { it.isLetterOrDigit() }) {
                    bot.sendMessage(
                        chatId = ChatId.fromId(message.chat.id),
                        text = "Название команды может содержать только цифры и буквы.",
                        disableNotification = true,
                        replyToMessageId = message.messageId,
                        replyMarkup = ReplyKeyboardRemove()
                    )
                    return@command
                }
                val maxTeamNameLength = 13
                if (teamName.length > maxTeamNameLength) {
                    bot.sendMessage(
                        chatId = ChatId.fromId(message.chat.id),
                        text = "Название команды может содержать не более $maxTeamNameLength-ти букв.",
                        disableNotification = true,
                        replyToMessageId = message.messageId,
                        replyMarkup = ReplyKeyboardRemove()
                    )
                    return@command
                }
                runBlocking {
                    factory.createTeamUseCase(message.chat.id, teamName).fold({
                        bot.sendMessage(
                            chatId = ChatId.fromId(message.chat.id),
                            text = "${message.markdownTag} создал(а) команду: «`$teamName`».",
                            parseMode = ParseMode.MARKDOWN,
                            disableNotification = true,
                            replyToMessageId = message.messageId,
                            replyMarkup = ReplyKeyboardRemove()
                        )
                    }, {
                        message.logError(bot, it)
                        return@runBlocking
                    })
                    val chatRoom = factory.getChatRoomUseCase(message.chat.id).getOrElse {
                        message.logError(bot, it)
                        return@runBlocking
                    }
                    val teamsAsCommand = chatRoom.sortedTeams.map { BotCommand(it.name, it.name) }
                    val commands = bot.getMyCommands().get().plus(teamsAsCommand).distinctBy { it.command }
                    bot.setMyCommands(commands)
                }
            }
        }

        command("delete") {
            update.consume()
            message.runIfOwner(bot, appConfig.ownerId) {
                val teamName = args.joinToString(separator = " ") { it.replace(ICON_PROTECT, ' ').trim() }
                if (teamName.isBlank()) {
                    bot.sendMessage(
                        chatId = ChatId.fromId(message.chat.id),
                        text = "Команды с таким именем не найдено.",
                        disableNotification = true,
                        replyToMessageId = message.messageId,
                        replyMarkup = ReplyKeyboardRemove()
                    )
                    return@command
                }
                runBlocking {
                    val chatRoom = factory.getChatRoomUseCase(message.chat.id).getOrElse {
                        message.logError(bot, it)
                        return@runBlocking
                    }
                    val team = chatRoom.getTeamBy(teamName).getOrElse {
                        message.logError(bot, it)
                        return@runBlocking
                    }
                    val session = client.startSession()
                    try {
                        session.startTransaction()
                        val result = factory.deleteTeamUseCase(chatRoom, team.id)
                        session.commitTransactionAndAwait()

                        result.fold({
                            bot.sendMessage(
                                chatId = ChatId.fromId(message.chat.id),
                                text = "${message.markdownTag} удалил(а) команду: «`$teamName`».",
                                parseMode = ParseMode.MARKDOWN,
                                disableNotification = true,
                                replyToMessageId = message.messageId,
                                replyMarkup = ReplyKeyboardRemove()
                            )
                        }, {
                            message.logError(bot, it)
                            return@runBlocking
                        })

                        val teamsAsCommand = chatRoom.sortedTeams
                            .toMutableList()
                            .also { it.remove(team) }
                            .map { BotCommand(it.name, it.name) }
                        val commands = bot.getMyCommands().get().plus(teamsAsCommand).distinctBy { it.command }
                        bot.setMyCommands(commands)
                    } catch (e: Throwable) {
                        message.logError(bot, e)
                    } finally {
                        session.close()
                    }
                }
            }
        }

        command("protect") {
            update.consume()
            val user = message.from ?: return@command
            message.runIfOwner(bot, appConfig.ownerId) {
                runBlocking {
                    val chatRoom = factory.getChatRoomUseCase(message.chat.id).getOrElse {
                        message.logError(bot, it)
                        return@runBlocking
                    }
                    when {
                        chatRoom.teams.isEmpty() -> message.teamsNotFound(bot)
                        else -> bot.sendMessage(
                            chatId = ChatId.fromId(message.chat.id),
                            text = "`$ACTION_PROTECT` ${message.markdownTag} выбери ответ из списка...",
                            parseMode = ParseMode.MARKDOWN,
                            disableNotification = true,
                            replyToMessageId = message.messageId,
                            replyMarkup = KeyboardReplyMarkup(
                                keyboard = buildTeamsKeyboard(chatRoom.sortedTeams),
                                resizeKeyboard = true,
                                selective = true,
                                oneTimeKeyboard = true
                            )
                        ).fold({ response ->
                            val resultMsg = response?.result ?: return@fold
                            saveMessage(message.chat.id, user.id, resultMsg.messageId)
                        })
                    }
                }
            }
        }

        command("teams") {
            update.consume()
            runBlocking {
                val chatRoom = factory.getChatRoomUseCase(message.chat.id).getOrElse {
                    message.logError(bot, it)
                    return@runBlocking
                }
                when {
                    chatRoom.teams.isEmpty() -> message.teamsNotFound(bot)
                    else -> bot.sendMessage(
                        chatId = ChatId.fromId(message.chat.id),
                        text = "Список всех команд в чате:\n${chatRoom.readableTeams()}",
                        parseMode = ParseMode.MARKDOWN,
                        disableNotification = true,
                        replyToMessageId = message.messageId,
                        replyMarkup = ReplyKeyboardRemove()
                    )
                }
            }
        }

        command("team_members") {
            update.consume()
            val user = message.from ?: return@command
            runBlocking {
                val chatRoom = factory.getChatRoomUseCase(message.chat.id).getOrElse {
                    message.logError(bot, it)
                    return@runBlocking
                }

                if (chatRoom.teams.isEmpty()) message.teamsNotFound(bot)
                else bot.sendMessage(
                    chatId = ChatId.fromId(message.chat.id),
                    text = "`$ACTION_LIST` ${message.markdownTag} выбери ответ из списка...",
                    parseMode = ParseMode.MARKDOWN,
                    disableNotification = true,
                    replyToMessageId = message.messageId,
                    replyMarkup = KeyboardReplyMarkup(
                        keyboard = buildTeamsKeyboard(chatRoom.sortedTeams),
                        resizeKeyboard = true,
                        selective = true,
                        oneTimeKeyboard = true
                    )
                ).fold({ response ->
                    val resultMsg = response?.result ?: return@fold
                    saveMessage(message.chat.id, user.id, resultMsg.messageId)
                })
            }
        }

        command("i") {
            update.consume()
            val user = message.from ?: return@command
            runBlocking {
                val roomId = message.chat.id.toId<RoomId>()
                val member = factory.getMemberByUserUseCase(user).getOrElse {
                    message.logError(bot, it)
                    return@runBlocking
                }
                val teams = member.teamsIn(roomId)
                if (teams.isEmpty()) message.teamsForMemberNotFound(bot)
                else bot.sendMessage(
                    chatId = ChatId.fromId(message.chat.id),
                    text = "Вы состоите в командах:\n${member.readableTeams(roomId)}",
                    parseMode = ParseMode.MARKDOWN,
                    replyToMessageId = message.messageId,
                    replyMarkup = ReplyKeyboardRemove()
                )
            }
        }

        command("join") {
            update.consume()
            val user = message.from ?: return@command
            runBlocking {
                val member = factory.getMemberByUserUseCase(user).getOrElse {
                    message.logError(bot, it)
                    return@runBlocking
                }

                val chatRoom = factory.getChatRoomUseCase(message.chat.id).getOrElse {
                    message.logError(bot, it)
                    return@runBlocking
                }

                if (chatRoom.teams.isEmpty()) {
                    message.teamsNotFound(bot)
                    return@runBlocking
                }

                //Fixme: Potential performance issue
                val referenceIds: Set<Id<TeamId>> = member.teams.map { it.id }.toSet()
                val targetTeams = chatRoom.sortedTeams.filter { it.id !in referenceIds }

                if (targetTeams.isEmpty()) bot.sendMessage(
                    chatId = ChatId.fromId(message.chat.id),
                    text = "Вы уже состоите во всех командах.",
                    disableNotification = true,
                    replyToMessageId = message.messageId,
                    replyMarkup = ReplyKeyboardRemove()
                )
                else bot.sendMessage(
                    chatId = ChatId.fromId(message.chat.id),
                    text = "`$ACTION_JOIN` ${message.markdownTag} выбери ответ из списка...",
                    parseMode = ParseMode.MARKDOWN,
                    disableNotification = true,
                    replyToMessageId = message.messageId,
                    replyMarkup = KeyboardReplyMarkup(
                        keyboard = buildTeamsKeyboard(targetTeams),
                        resizeKeyboard = true,
                        selective = true,
                        oneTimeKeyboard = true
                    )
                ).fold({ response ->
                    val resultMsg = response?.result ?: return@fold
                    saveMessage(message.chat.id, user.id, resultMsg.messageId)
                })
            }
        }

        command("leave") {
            update.consume()
            val user = message.from ?: return@command
            val roomId = message.chat.id.toId<RoomId>()
            runBlocking {
                val member = factory.getMemberByUserUseCase(user).getOrElse {
                    message.logError(bot, it)
                    return@runBlocking
                }
                if (member.teams.isEmpty()) {
                    message.teamsForMemberNotFound(bot)
                    return@runBlocking
                }
                bot.sendMessage(
                    chatId = ChatId.fromId(message.chat.id),
                    text = "`$ACTION_LEAVE` ${message.markdownTag} выбери ответ из списка...",
                    parseMode = ParseMode.MARKDOWN,
                    disableNotification = true,
                    replyToMessageId = message.messageId,
                    replyMarkup = KeyboardReplyMarkup(
                        keyboard = buildMemberTeamsKeyboard(member.teamsIn(roomId).sortedBy { it.name }),
                        resizeKeyboard = true,
                        selective = true,
                        oneTimeKeyboard = true
                    )
                ).fold({ response ->
                    val resultMsg = response?.result ?: return@fold
                    saveMessage(message.chat.id, user.id, resultMsg.messageId)
                })
            }
        }

        command("tag") {
            update.consume()
            return@command
            val user = message.from ?: return@command
            runBlocking {
                val chatRoom = factory.getChatRoomUseCase(message.chat.id).getOrElse {
                    message.logError(bot, it)
                    return@runBlocking
                }

                if (chatRoom.teams.isEmpty()) {
                    message.teamsNotFound(bot)
                    return@runBlocking
                }

                //Fixme: Potential performance issue
                val referenceIds: Set<Id<TeamId>> = factory.getAllMembersInChatRoomUseCase(chatRoom).getOrElse {
                    message.logError(bot, it)
                    return@runBlocking
                }.flatMap { it.teams }.distinctBy { it.id }.map { it.id }.toSet()

                val targetTeams = chatRoom.sortedTeams.filter { it.id in referenceIds }

                when {
                    targetTeams.isEmpty() -> bot.sendMessage(
                        chatId = ChatId.fromId(message.chat.id),
                        text = "Все команды без участников - пустые.",
                        disableNotification = true,
                        replyToMessageId = message.messageId,
                        replyMarkup = ReplyKeyboardRemove()
                    )
                    else -> bot.sendMessage(
                        chatId = ChatId.fromId(message.chat.id),
                        text = "`$ACTION_TAG` ${message.markdownTag} выбери ответ из списка...",
                        parseMode = ParseMode.MARKDOWN,
                        disableNotification = true,
                        replyToMessageId = message.messageId,
                        replyMarkup = KeyboardReplyMarkup(
                            keyboard = buildTeamsKeyboard(targetTeams),
                            resizeKeyboard = true,
                            selective = true,
                            oneTimeKeyboard = true
                        )
                    ).fold({ response ->
                        val resultMsg = response?.result ?: return@fold
                        saveMessage(message.chat.id, user.id, resultMsg.messageId)
                    })
                }
            }
        }

        //Answers handler
        message(Filter.Custom { this.replyToMessage != null && replyToMessage?.entities?.any { this.from?.id == it.user?.id } == true }) {
            update.consume()
            val action = message.replyToMessage?.text?.split(" ")?.firstOrNull() ?: return@message
            val teamName = message.text?.replace(ICON_PROTECT, ' ')?.trim() ?: return@message
            val user = message.from ?: return@message
            runBlocking {
                val member = factory.getMemberByUserUseCase(user).getOrElse {
                    message.logError(bot, it)
                    return@runBlocking
                }
                if (teamName == KEY_CANCEL) {
                    bot.sendMessage(
                        chatId = ChatId.fromId(message.chat.id),
                        text = "Действие отменено.",
                        replyToMessageId = message.messageId,
                        replyMarkup = ReplyKeyboardRemove()
                    ).fold({
                        bot.saveAndDeleteMessages(message.chat.id, user.id, message.messageId)
                    })
                    return@runBlocking
                }
                when (action) {
                    ACTION_TAG -> {
                        val membersToTag = factory.getTeamMembersUseCase(message.chat.id, teamName).getOrElse {
                            message.logError(bot, it)
                            return@runBlocking
                        }

                        bot.sendMessage(
                            chatId = ChatId.fromId(message.chat.id),
                            text = "«`$teamName`» ${tagMembers(membersToTag)}",
                            parseMode = ParseMode.MARKDOWN,
                            replyMarkup = ReplyKeyboardRemove()
                        ).fold({
                            bot.saveAndDeleteMessages(message.chat.id, user.id, message.messageId)
                        })
                    }
                    ACTION_LIST -> {
                        val teamMembers = factory.getTeamMembersUseCase(message.chat.id, teamName).getOrElse {
                            message.logError(bot, it)
                            return@runBlocking
                        }
                        bot.sendMessage(
                            chatId = ChatId.fromId(message.chat.id),
                            text = listMembers(teamName, teamMembers),
                            parseMode = ParseMode.MARKDOWN,
                            replyMarkup = ReplyKeyboardRemove()
                        ).fold({
                            bot.saveAndDeleteMessages(message.chat.id, user.id, message.messageId)
                        })
                    }
                    ACTION_JOIN -> factory.joinTeamUseCase(message.chat.id, teamName, member).fold({
                        bot.sendMessage(
                            chatId = ChatId.fromId(message.chat.id),
                            text = "${message.markdownTag} присоединился к команде «`$teamName`».",
                            parseMode = ParseMode.MARKDOWN,
                            replyMarkup = ReplyKeyboardRemove()
                        ).fold({
                            bot.saveAndDeleteMessages(message.chat.id, user.id, message.messageId)
                        })
                    }, {
                        message.logError(bot, it)
                    })
                    ACTION_LEAVE -> {
                        val chatRoom = factory.getChatRoomUseCase(message.chat.id).getOrElse {
                            message.logError(bot, it)
                            return@runBlocking
                        }
                        val team = chatRoom.getTeamBy(teamName).getOrElse {
                            message.logError(bot, it)
                            return@runBlocking
                        }
                        factory.leaveTeamUseCase(team.id, member).fold({
                            bot.sendMessage(
                                chatId = ChatId.fromId(message.chat.id),
                                text = "${message.markdownTag} покинул команду «`$teamName`».",
                                parseMode = ParseMode.MARKDOWN,
                                replyMarkup = ReplyKeyboardRemove()
                            ).fold({
                                bot.saveAndDeleteMessages(message.chat.id, user.id, message.messageId)
                            })
                        }, {
                            message.logError(bot, it)
                        })
                    }
                    ACTION_PROTECT -> {
                        val session = client.startSession()
                        try {
                            session.startTransaction()
                            val result = factory.toggleProtectTeamUseCase(
                                message.chat.id,
                                teamName,
                                member
                            )
                            session.commitTransactionAndAwait()
                            result.fold({ team ->
                                val text =
                                    if (team.isProtected) "Включена защищена от изменений для команды «`${team.name}`»."
                                    else "Защищена от изменений отключена для команды «`${team.name}`» ."
                                bot.sendMessage(
                                    chatId = ChatId.fromId(message.chat.id),
                                    text = text,
                                    parseMode = ParseMode.MARKDOWN,
                                    replyMarkup = ReplyKeyboardRemove()
                                ).fold({
                                    bot.saveAndDeleteMessages(message.chat.id, user.id, message.messageId)
                                })
                            }, {
                                message.logError(bot, it)
                            })
                        } catch (e: Throwable) {
                            message.logError(bot, e)
                        } finally {
                            session.close()
                        }
                    }
                }
            }
        }

        message(Filter.Custom { text != null && text?.startsWith("/") == true }) {
            update.consume()
            val user = message.from ?: return@message
            val command = message.text?.let { it.drop(1).split(" ", "\n")[0].split("@")[0].replace(ICON_PROTECT, ' ') }
                ?: return@message

            runBlocking {
                val membersToTag = factory.getTeamMembersUseCase(message.chat.id, command).getOrElse {
                    message.logError(bot, it)
                    return@runBlocking
                }

                bot.sendMessage(
                    chatId = ChatId.fromId(message.chat.id),
                    text = tagMembers(membersToTag),
                    parseMode = ParseMode.MARKDOWN,
                    replyToMessageId = message.messageId,
                    replyMarkup = ReplyKeyboardRemove()
                )
            }
        }

        newChatMembers {
            message.leftChatMember?.id?.let { userId ->
                runBlocking {
                    val member = factory.getMemberByIdUseCase(userId).getOrElse {
                        message.logError(bot, it)
                        return@runBlocking
                    }
                    factory.leaveChatRoomUseCase(message.chat.id, member)
                }
            }
        }

        telegramError {
            logger.error("TelegramError: ${error.getErrorMessage()}")
        }
    }
}


fun saveMessage(roomId: RoomId, memberId: MemberId, messageId: MessageId) {
    temporalMessageStorage
        .getOrPut(roomId) { mutableMapOf() }
        .getOrPut(memberId) { mutableListOf() }
        .add(messageId)
}

fun Bot.saveAndDeleteMessages(roomId: RoomId, memberId: MemberId, messageId: MessageId) {
    val chatId = ChatId.fromId(roomId)
    temporalMessageStorage
        .getOrPut(roomId) { mutableMapOf() }
        .getOrPut(memberId) { mutableListOf() }
        .apply {
            add(messageId)
            forEach { id ->
                deleteMessage(chatId, id)
            }
            clear()
        }
}

fun Message.teamsForMemberNotFound(bot: Bot) {
    bot.sendMessage(
        chatId = ChatId.fromId(chat.id),
        text = "Вы не состоите ни в одной из команд.",
        disableNotification = true,
        replyToMessageId = messageId,
        replyMarkup = ReplyKeyboardRemove()
    )
}

fun Message.teamsNotFound(bot: Bot) {
    bot.sendMessage(
        chatId = ChatId.fromId(chat.id),
        text = "В чате нет команд.",
        disableNotification = true,
        replyToMessageId = messageId,
        replyMarkup = ReplyKeyboardRemove()
    )
}

fun Message.logError(bot: Bot, error: Throwable) {
    when (error) {
        is MemberNotFound -> {
        }
        is TeamNotFound -> bot.sendMessage(
            chatId = ChatId.fromId(chat.id),
            text = "Команды «`${error.teamName}`» не существует.",
            parseMode = ParseMode.MARKDOWN,
            disableNotification = true,
            replyToMessageId = messageId,
            replyMarkup = ReplyKeyboardRemove()
        )
        is TeamAlreadyExists -> bot.sendMessage(
            chatId = ChatId.fromId(chat.id),
            text = "Команда с таким именем уже существует.",
            parseMode = ParseMode.MARKDOWN,
            disableNotification = true,
            replyToMessageId = messageId,
            replyMarkup = ReplyKeyboardRemove()
        )
        is ChatRoomTeamLimit -> bot.sendMessage(
            chatId = ChatId.fromId(chat.id),
            text = "Добавление новой команды невозможно, лимит $MAX_TEAMS_PER_CHAT команд в одном чате.",
            disableNotification = true,
            replyToMessageId = messageId,
            replyMarkup = ReplyKeyboardRemove()
        )
        is ChatRoomNotFound -> bot.sendMessage(
            chatId = ChatId.fromId(chat.id),
            text = "Ошибка, данный чат требует регистрации!",
            disableNotification = true,
            replyToMessageId = messageId,
            replyMarkup = ReplyKeyboardRemove()
        )
        is ChatRoomAlreadyExists -> bot.sendMessage(
            chatId = ChatId.fromId(chat.id),
            text = "Текущий чат уже зарегистрирован.",
            disableNotification = true,
            replyToMessageId = messageId,
            replyMarkup = ReplyKeyboardRemove()
        )
        else -> bot.sendMessage(
            chatId = ChatId.fromId(chat.id),
            text = error.localizedMessage ?: error.message ?: error.toString(),
            disableNotification = true,
            replyToMessageId = messageId,
            replyMarkup = ReplyKeyboardRemove()
        )
    }
}

fun buildMemberTeamsKeyboard(teams: List<Member.Team>): List<List<KeyboardButton>> {
    val board = teams.groupBy { it.isProtected }
    val protected =
        board[true]?.map { KeyboardButton("${it.name}$ICON_PROTECT") }?.chunked(TEAMS_PER_ROW) ?: emptyList()
    val other = board[false]?.map { KeyboardButton(it.name) }?.chunked(TEAMS_PER_ROW) ?: emptyList()
    val cancel = listOf(listOf(KeyboardButton(KEY_CANCEL)))
    return protected + other + cancel
}

fun buildTeamsKeyboard(teams: List<ChatRoom.Team>): List<List<KeyboardButton>> {
    val board = teams.groupBy { it.isProtected }
    val protected =
        board[true]?.map { KeyboardButton("${it.name}$ICON_PROTECT") }?.chunked(TEAMS_PER_ROW) ?: emptyList()
    val other = board[false]?.map { KeyboardButton(it.name) }?.chunked(TEAMS_PER_ROW) ?: emptyList()
    val cancel = listOf(listOf(KeyboardButton(KEY_CANCEL)))
    return protected + other + cancel
}