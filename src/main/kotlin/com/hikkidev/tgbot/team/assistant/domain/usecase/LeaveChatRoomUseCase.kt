/*
 * Copyright 2021, Sora Nai
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.hikkidev.tgbot.team.assistant.domain.usecase

import com.hikkidev.tgbot.team.assistant.data.model.Member
import com.hikkidev.tgbot.team.assistant.data.model.RoomId
import com.hikkidev.tgbot.team.assistant.data.repository.MemberRepository
import com.hikkidev.tgbot.team.assistant.domain.model.toId


typealias LeaveChatRoomUseCase = suspend (roomId: RoomId, member: Member) -> Result<Unit>


//Fixme: Potential performance issue
suspend inline fun leaveChatRoom(
    roomId: RoomId,
    member: Member,
    crossinline leaveTeamUseCase: LeaveTeamUseCase,
    repository: MemberRepository
): Result<Unit> = runCatching {
    val id = roomId.toId<RoomId>()
    member.teams
        .filter { it.chatId == id }
        .forEach {
            leaveTeamUseCase(it.id, member)
        }
    repository.delete(member.id)
}