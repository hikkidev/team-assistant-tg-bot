/*
 * Copyright 2021, Sora Nai
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.hikkidev.tgbot.team.assistant.data.repository

import com.hikkidev.tgbot.team.assistant.data.model.Member
import com.hikkidev.tgbot.team.assistant.data.model.MemberId
import org.bson.conversions.Bson
import org.litote.kmongo.Id
import org.litote.kmongo.coroutine.CoroutineDatabase

class MemberRepositoryImpl(private val database: CoroutineDatabase) : MemberRepository {

    override suspend fun find(id: Id<MemberId>): Member? {
        return database
            .getCollection<Member>()
            .findOneById(id)
    }

    override suspend fun find(filter: Bson): List<Member> {
        return database
            .getCollection<Member>()
            .find(filter)
            .toList()
    }

    override suspend fun update(member: Member): Member {
        database
            .getCollection<Member>()
            .save(member)
        return member
    }

    override suspend fun delete(id: Id<MemberId>) {
        database
            .getCollection<Member>()
            .deleteOneById(id)
    }
}