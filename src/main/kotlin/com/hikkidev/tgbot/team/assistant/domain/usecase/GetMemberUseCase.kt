/*
 * Copyright 2021, Sora Nai
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.hikkidev.tgbot.team.assistant.domain.usecase

import com.github.kotlintelegrambot.entities.User
import com.hikkidev.tgbot.team.assistant.data.model.Member
import com.hikkidev.tgbot.team.assistant.data.model.MemberId
import com.hikkidev.tgbot.team.assistant.data.repository.MemberRepository
import com.hikkidev.tgbot.team.assistant.domain.error.MemberNotFound
import com.hikkidev.tgbot.team.assistant.domain.model.toId


typealias GetMemberByUserUseCase = suspend (user: User) -> Result<Member>
typealias GetMemberByIdUseCase = suspend (memberId: MemberId) -> Result<Member>

suspend inline fun getMemberByUser(
    user: User,
    repository: MemberRepository
): Result<Member> = runCatching {
    val id = user.id.toId<MemberId>()
    val fullName = user.lastName?.let { "${user.firstName} $it" } ?: user.firstName

    when (val result = repository.find(id)) {
        null -> {
            val member = Member(id, fullName, emptyList())
            repository.update(member)
        }
        else -> result
    }
}

suspend inline fun getMemberById(
    memberId: MemberId,
    repository: MemberRepository
): Result<Member> = runCatching {
    val id = memberId.toId<MemberId>()
    when (val result = repository.find(id)) {
        null -> throw MemberNotFound()
        else -> result
    }
}