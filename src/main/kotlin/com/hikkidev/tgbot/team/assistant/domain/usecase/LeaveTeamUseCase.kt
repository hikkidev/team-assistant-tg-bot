/*
 * Copyright 2021, Sora Nai
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.hikkidev.tgbot.team.assistant.domain.usecase

import com.hikkidev.tgbot.team.assistant.data.model.Member
import com.hikkidev.tgbot.team.assistant.data.model.TeamId
import com.hikkidev.tgbot.team.assistant.data.repository.MemberRepository
import org.litote.kmongo.Id


typealias LeaveTeamUseCase = suspend (teamId: Id<TeamId>, member: Member) -> Result<Member>

suspend inline fun leaveTheTeam(
    teamId: Id<TeamId>,
    member: Member,
    repository: MemberRepository
): Result<Member> = runCatching {
    val teams = member.teams.toMutableList()
    if (teams.removeIf { team -> team.id == teamId }) {
        val newRecord = member.copy(teams = teams)
        repository.update(newRecord)
    }
    member
}