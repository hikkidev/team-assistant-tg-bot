/*
 * Copyright 2021, Sora Nai
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.hikkidev.tgbot.team.assistant.data.model

import com.hikkidev.tgbot.team.assistant.domain.error.ChatRoomNotFound
import com.hikkidev.tgbot.team.assistant.domain.error.TeamNotFound
import kotlinx.serialization.Contextual
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.litote.kmongo.Id

@Serializable
data class ChatRoom(
    @Contextual
    @SerialName("_id")
    val id: Id<RoomId>,
    val teams: List<Team>
) {

    val sortedTeams: List<Team> get() = teams.sortedBy { it.name }

    fun readableTeams(): String {
        return teams
            .sortedBy { it.name }
            .mapIndexed { index, team -> "${index + 1}) `${team.name}`" }
            .joinToString("\n")
    }

    @Throws(ChatRoomNotFound::class)
    fun getTeamBy(teamName: String) = runCatching {
        when (val team = teams.find { it.name.equals(teamName, ignoreCase = true) }) {
            null -> throw TeamNotFound(teamName)
            else -> team
        }
    }

    @Serializable
    data class Team(
        @Contextual
        @SerialName("_id")
        val id: Id<TeamId>,
        val name: String,
        val isProtected: Boolean = false
    )
}
