/*
 * Copyright 2021, Sora Nai
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.hikkidev.tgbot.team.assistant.presentation

class AppConfig {
    val hostIp = "SERVER_IP".getOrThrow("Host domain requires")
    val port = 443

    val keyAlias = "JKS_KEY_ALIAS".getOrThrow("Key alias requires")
    val privateKeyPassword = "JKS_KEY_PASSWORD".getOrThrow("Private key password requires")

    val ownerId = System.getenv("BOT_OWNER_ID")?.toLong() ?: 0L
    val botToken = "BOT_TOKEN".getOrThrow("Telegram token requires")

    val dbHost = System.getenv("DB_HOST") ?: "localhost"
    val dbName = System.getenv("DB_NAME") ?: "qualia_db"
    val dbConnectionString = "mongodb://${dbHost}:27017"

    val repoUrl = System.getenv("REPO_URL") ?: "https://gitlab.com/"
}

fun String.getOrThrow(errorMsg: String): String {
    val value = System.getenv(this)
    if (value.isNullOrBlank()) throw IllegalArgumentException(errorMsg)
    return value
}