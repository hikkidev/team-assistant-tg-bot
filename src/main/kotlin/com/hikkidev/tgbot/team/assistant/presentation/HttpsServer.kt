/*
 * Copyright 2021, Sora Nai
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.hikkidev.tgbot.team.assistant.presentation

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*

class HttpsServer(
    private val appConfig: AppConfig,
    private val certUtils: CertificateUtils
) {

    private lateinit var server: NettyApplicationEngine

    fun createServer(lambda: (String) -> Unit) {
        server = embeddedServer(Netty, applicationEngineEnvironment {
            sslConnector(
                keyStore = certUtils.keyStore,
                keyAlias = certUtils.keyAlias,
                keyStorePassword = { certUtils.keyStorePassword },
                privateKeyPassword = { certUtils.privateKeyPassword }) {
                module {
                    routing {
                        post("/${appConfig.botToken}") {
                            val response = call.receiveText()
                            if (response.isNotBlank()) lambda.invoke(response)
                            call.respond(HttpStatusCode.OK)
                        }
                    }
                }
                port = appConfig.port
                keyStorePath = certUtils.keyStoreFile.absoluteFile
                host = "0.0.0.0"
            }
        })
    }

    fun start() {
        server.start(wait = true)
    }
}

