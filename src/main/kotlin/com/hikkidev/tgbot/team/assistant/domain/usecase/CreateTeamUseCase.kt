/*
 * Copyright 2021, Sora Nai
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.hikkidev.tgbot.team.assistant.domain.usecase

import com.hikkidev.tgbot.team.assistant.data.model.ChatRoom
import com.hikkidev.tgbot.team.assistant.data.model.RoomId
import com.hikkidev.tgbot.team.assistant.data.repository.ChatRoomRepository
import com.hikkidev.tgbot.team.assistant.domain.error.ChatRoomTeamLimit
import com.hikkidev.tgbot.team.assistant.domain.error.TeamAlreadyExists
import org.litote.kmongo.newId

const val MAX_TEAMS_PER_CHAT = 12
const val TEAMS_PER_ROW = 3

typealias CreateTeamUseCase = suspend (roomId: RoomId, teamName: String) -> Result<ChatRoom>

suspend inline fun createTeam(
    roomId: RoomId,
    teamName: String,
    crossinline getChatRoomUseCase: GetChatRoomUseCase,
    repository: ChatRoomRepository
): Result<ChatRoom> = runCatching {
    val chatRoom = getChatRoomUseCase(roomId).getOrThrow()
    if (chatRoom.teams.size >= MAX_TEAMS_PER_CHAT) throw ChatRoomTeamLimit()

    if (chatRoom.teams.find { it.name == teamName } == null) {
        val team = ChatRoom.Team(newId(), teamName)
        val newRecord = chatRoom.copy(teams = chatRoom.teams.plus(team))
        repository.update(newRecord)
    } else throw TeamAlreadyExists()
}