/*
 * Copyright 2021, Sora Nai
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.hikkidev.tgbot.team.assistant.presentation

import java.io.File
import java.io.FileInputStream
import java.security.KeyStore

class CertificateUtils(appConfig: AppConfig) {
    val privateKeyPassword = appConfig.privateKeyPassword.toCharArray()
    val keyStorePassword = appConfig.privateKeyPassword.toCharArray()
    val keyAlias = appConfig.keyAlias

    val keyStoreFile: File
        get() = File(keyStorePath).let { file ->
            if (file.exists() || file.isAbsolute)
                file
            else throw IllegalArgumentException("KeyStore not found at $keyStorePath")
        }

    val keyStore: KeyStore
        get() = KeyStore.getInstance("JKS").apply {
            FileInputStream(keyStoreFile).use {
                load(it, privateKeyPassword)
            }
            requireNotNull(getKey(keyAlias, privateKeyPassword) == null) {
                "The specified key $keyAlias doesn't exist in the key store $keyStoreFile}"
            }
        }

    val certPath = "/home/url_cert.pem"
    private val keyStorePath = "/home/ssl-keystore.jks"
}