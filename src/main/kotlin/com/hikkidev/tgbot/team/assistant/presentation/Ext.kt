/*
 * Copyright 2021, Sora Nai
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.hikkidev.tgbot.team.assistant.presentation

import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.entities.Message
import com.hikkidev.tgbot.team.assistant.data.model.Member

inline fun Message.runIfOwner(bot: Bot, ownerId: Long, block: () -> Unit) {
//    bot.sendMessage(
//        chatId = ChatId.fromId(chat.id),
//        text = "[DEBUG_MODE] Инструкция исполняется от имени администратора!"
//    )
//    block()
    if (from?.id == ownerId) block() else bot.sendMessage(
        chatId = ChatId.fromId(chat.id),
        text = "Только создатель бота имеет право использовать эту инструкцию!"
    )
}

val Message.markdownTag: String
    get() = "[${from?.firstName}](tg://user?id=${from?.id})"


fun tagMembers(members: List<Member>): String =
    members.joinToString { member -> "[${member.name}](tg://user?id=${member.id})" }

fun listMembers(teamName: String, members: List<Member>): String {
    if (members.isEmpty()) return "В команде «`$teamName`» пока никого нет."
    return "В команде «`$teamName`» состоят:\n" + members
        .sortedBy { it.name }
        .mapIndexed { index, member -> "${index + 1}) `${member.name}`" }
        .joinToString("\n")
}
