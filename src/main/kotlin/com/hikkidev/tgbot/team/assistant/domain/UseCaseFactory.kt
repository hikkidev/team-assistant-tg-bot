/*
 * Copyright 2021, Sora Nai
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.hikkidev.tgbot.team.assistant.domain

import com.hikkidev.tgbot.team.assistant.data.repository.ChatRoomRepository
import com.hikkidev.tgbot.team.assistant.data.repository.MemberRepository
import com.hikkidev.tgbot.team.assistant.domain.usecase.*

class UseCaseFactory(
    private val chatRoomRepository: ChatRoomRepository,
    private val memberRepository: MemberRepository
) {
    /**
     * Require transaction
     */
    val toggleProtectTeamUseCase: ToggleProtectTeamUseCase
        get() = { roomId, teamName, member ->
            toggleProtectTeam(roomId, teamName, member, getChatRoomUseCase, memberRepository, chatRoomRepository)
        }

    val getTeamMembersUseCase: GetTeamMembersUseCase
        get() = { roomId, teamName ->
            getTeamMembers(roomId, teamName, getChatRoomUseCase, memberRepository)
        }

    //region Chat Room
    val registerChatRoomUseCase: RegisterChatRoomUseCase
        get() = { roomId ->
            registerChatRoom(roomId, chatRoomRepository)
        }

    val getChatRoomUseCase: GetChatRoomUseCase
        get() = { roomId ->
            getChatRoom(roomId, chatRoomRepository)
        }

    val createTeamUseCase: CreateTeamUseCase
        get() = { roomId, teamName ->
            createTeam(roomId, teamName, getChatRoomUseCase, chatRoomRepository)
        }

    val leaveChatRoomUseCase: LeaveChatRoomUseCase
        get() = { roomId, member ->
            leaveChatRoom(roomId, member, leaveTeamUseCase, memberRepository)
        }

    /**
     * Require transaction
     */
    val deleteTeamUseCase: DeleteTeamUseCase
        get() = { chatRoom, teamId ->
            deleteTeam(chatRoom, teamId, leaveTeamUseCase, chatRoomRepository, memberRepository)
        }

    //endregion

    //region Member
    val getAllMembersInChatRoomUseCase: GetAllMembersInChatRoomUseCase
        get() = { room ->
            getAllMembersInChatRoom(room, memberRepository)
        }

    val joinTeamUseCase: JoinTeamUseCase
        get() = { roomId, teamName, member ->
            joinTheTeam(roomId, teamName, member, getChatRoomUseCase, memberRepository)
        }

    val leaveTeamUseCase: LeaveTeamUseCase
        get() = { team, member ->
            leaveTheTeam(team, member, memberRepository)
        }

    val getMemberByUserUseCase: GetMemberByUserUseCase
        get() = { user ->
            getMemberByUser(user, memberRepository)
        }

    val getMemberByIdUseCase: GetMemberByIdUseCase
        get() = { id ->
            getMemberById(id, memberRepository)
        }
    //endregion
}