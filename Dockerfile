FROM openjdk:8-jdk
RUN mkdir /app
COPY ./build/install/TeamAssistantBot /app/
WORKDIR /app/bin
CMD ["./TeamAssistantBot"]