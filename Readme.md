# \[ 🚧 Work in progress 🚧 \] QualiaBot 1.1

## Цель бота:

Успростить упоминание(тег) множества людей одновременно.

## Содержание

1. [Функционал](#main)
2. [Как работают теги?](#tags)
3. [Как присоединиться к команде?](#join)
4. [Как покинуть команду?](#leave)

## Функционал: <a name="main"></a>

* Тегнуть всех членков команды через инструкцию: `/название команды` __В НАЧАЛЕ СООБЩЕНИЯ__, см. работу тегов
* `/i` Показать список команд в которых __вы состоите__
* `/teams` Показать все существующие команды в чате
* `/team_members` Показать всех участников определенной команды
* `/join` Присоединиться к определенной команде
* `/leave` Покинуть определенную команду

## Как работают теги? <a name="tags"></a>

Администратор создает определенные команды в чате к которым присоединяются участники чата.

Предположим, что создано две команды:

1) Android
2) IOS

Попробуем тегнуть всех людей из группы iOS:

> Note: Название команды не чувствительно к регистру, IoS = iOS = ios и т.д.

![tag](images/tag.png)

* 👍`/ios` - Works
* 👍`/ios Ребята крутую штуку нашел, гляньте https://gitlab.com/` - Works
* 🛑`ios` - __не сработает__, так как это обычный текст, а не инструкция
* 🛑`Господа и дамы из /ios есть инфа о ...` - __не сработает__, так как сообщение __должно__ начинатсья со `/`
* 🛑 Если сообщение будет содержать вложения (изображения, файлы и т.д.) инструкция будет проигнорирована!

## Как присоединиться к команде? <a name="join"></a>

1. Проверь список доступных команд через инструкцию `/teams`
2. Затем напиши `/join`, откроется список доступных команд. __Нажми на кнопку__ с названием команды к которой хочешь
   присоединиться.

![join](images/join_begin_1.png)

3. После, в чате появится уведомление.

![join result](images/join_end_1.png)

> Note: команды в которых вы состоите не отображаются в списке `/join`

![join new](images/join_begin_2.png)

## Как покинуть команду? <a name="leave"></a>

1. Используй инструкцию `/leave`
2. Откроется список команд в которых ты состоишь. __Нажми на кнопку__ с названием команды которую хочешь покинуть.

![leave](images/leave_begin.png)

3. После, в чате появится уведомление.

![leave result](images/leave_end.png)

---
